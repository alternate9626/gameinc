﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    class BowWeapon : Weapon
    {
       public String Name { get;  private set; }
       public int MaxDamage { get; private set; }
       public int MinDamage { get; private set; }

        public BowWeapon()
        {
            this.Name = "Bow";
            this.MaxDamage = 6;
            this.MinDamage = 1;
        }
    }
}
