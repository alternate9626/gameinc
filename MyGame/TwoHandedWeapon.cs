﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    class TwoHandedWeapon : Weapon
    {
        public String Name { get; private set; }
        public int MaxDamage { get; private set; }
        public int MinDamage { get; private set; }

        public TwoHandedWeapon()
        {
            this.Name = "Two Handed Weapon";
            this.MaxDamage = 20;
            this.MinDamage = 5;
        }
    }
}

