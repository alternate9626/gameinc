﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    class OrcMonster : Monster
    {
        public String Name { get; private set; }
        public int Armor { get; private set; }
        public int HitPoints { get; private set; }
        public static Weapon Weapon = new DaggerWeapon();
        public static Random Rand = new Random();
        public int damage { get; private set; }
        public int LastDamageDone { get; private set; }
        public int XpReward { get; private set; }
        public int Accuracy { get; private set; }

        public OrcMonster()
        {
            this.Name = "Orc";
            this.Armor = 1;
            this.HitPoints = 10;
            this.damage = Rand.Next(Weapon.MinDamage, Weapon.MaxDamage);
            this.XpReward = 200;
            this.Accuracy = 8;
        }

        public void AttackPlayer(Player player)
        {
            player.TakeDamage(damage);
            this.LastDamageDone = player.LastDamageDoneByMonster;
        }

        public void TakeDamage(int damageDone)
        {
            int totalDamage = damageDone - this.Armor;
            if (totalDamage < 0)
            {
                totalDamage = 0;
            }
            this.HitPoints = this.HitPoints - totalDamage;

            if (isDead())
            {
                this.HitPoints = 0;
            }
        }

        private bool isDead()
        {
            return this.HitPoints <= 0;
        }

        public void Kill()
        {
            this.HitPoints = 0;
        }
    }
}
