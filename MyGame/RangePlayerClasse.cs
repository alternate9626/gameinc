
using MyGame;
using System;
public class RangePlayerClasse : PlayerClasse
{
    public int Accuracy { get; private set; }
    public int HitPoints { get; private set; }
    public int MaxHitPoints { get; private set; }
    public int XP { get; private set; }
    public int XpForNextLevel { get; private set; }
    public int Level { get; private set; }
    public int Armor { get; private set; }
    public Weapon Weapon { get; private set; }
    public String Name { get; private set; }
    public int Damage { get; private set; }

    public RangePlayerClasse()
    {
        this.Accuracy = 5;
        this.HitPoints = 10;
        this.MaxHitPoints = 10;
        this.XP = 0;
        this.XpForNextLevel = 1000;
        this.Level = 1;
        this.Armor = 1;
        this.Weapon = new BowWeapon();
        this.Name = "Range";
        this.Damage = 10;
    }
}
