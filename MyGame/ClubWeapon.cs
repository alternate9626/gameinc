﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    class ClubWeapon : Weapon
    {
        public String Name { get; private set; }
        public int MaxDamage { get; private set; }
        public int MinDamage { get; private set; }

        public ClubWeapon()
        {
            this.Name = "Club";
            this.MaxDamage = 8;
            this.MinDamage = 3;
        }
    }
}
