using MyGame;
using System;
public class MeelePlayerClasse : PlayerClasse
{

    public int Accuracy { get; private set; }
    public int HitPoints { get; private set; }
    public int MaxHitPoints { get; private set; }
    public int XP { get; private set; }
    public int XpForNextLevel { get; private set; }
    public int Level { get; private set; }
    public int Armor { get; private set; }
    public Weapon Weapon { get; private set; }
    public String Name { get; private set; }
    public int Damage { get; private set; }

    public MeelePlayerClasse()
    {
        this.Accuracy = 10;
        this.HitPoints = 20;
        this.MaxHitPoints = 20;
        this.XP = 0;
        this.XpForNextLevel = 1000;
        this.Level = 1;
        this.Armor = 4;
        this.Weapon = new LongSwordWeapon();
        this.Name = "Meele";
        this.Damage = 7;
    }

}
