﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{using MyGame;
using System;
    class OgreMonster : Monster
    {
        public String Name { get; private set; }
        public int Armor { get; private set; }
        public int HitPoints { get; private set; }
        public static Weapon Weapon = new DaggerWeapon();
        public static Random Rand = new Random();
        public int damage { get; private set; }
        public int LastDamageDone { get; private set; }
        public int XpReward { get; private set; }
        public int Accuracy { get; private set; }

        public OgreMonster()
        {
            this.Name = "Ogre";
            this.Armor = 2;
            this.HitPoints = 20;
            this.damage = Rand.Next(Weapon.MinDamage, Weapon.MaxDamage);
            this.XpReward = 500;
            this.Accuracy = 12;
        }

        public void AttackPlayer(Player player)
        {
            player.TakeDamage(damage);
            this.LastDamageDone = player.LastDamageDoneByMonster;
        }        

        public void TakeDamage(int damageDone)
        {
            int totalDamage = damageDone - this.Armor;
            if (totalDamage < 0)
            {
                totalDamage = 0;
            }
            this.HitPoints = this.HitPoints - totalDamage;
            if (isDead())
            {
                this.HitPoints = 0;
            }
        }

        private bool isDead()
        {
            return this.HitPoints <= 0;
        } 
        
        public void Kill()
        {
            this.HitPoints = 0;
        }  
    }
}

