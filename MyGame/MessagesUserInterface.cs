

using System;
public class MessagesUserInterface : UserInterface
{
    public int GetUserAction()
    {
        int userAction = GetUserActionInput();
        this.ClearScreen();
        return userAction;
    }

    public int GetUserActionInput()
    {
        int userAction = Convert.ToInt32(Console.ReadLine());
        while (this.IsInputNotValidForClasse(userAction))
        {
            this.ClearScreen();
            Console.WriteLine("Wrong Choice.");
            this.ShowAvailableActions();
            userAction = Convert.ToInt32(Console.ReadLine());
        }
        return userAction;
    }

    public String GetUserName()
    {
        Console.WriteLine("Whats your name?");
        String name = Console.ReadLine();
        this.ClearScreen();
        return name;
    }

    public PlayerClasse GetUserClasse()
    {
        showClassOptions();
        return this.GetClasse();
    }

    public PlayerClasse GetClasse()
    {
        PlayerClasse classe = new MeelePlayerClasse();
        int userInput = GetInputForClasse();
        switch (userInput)
        {
            case 1:
                classe = new MeelePlayerClasse();
                break;
            case 2:
                classe = new RangePlayerClasse();
                break;
            case 3:
                classe = new HealerPlayerClasse();
                break;
        }
        return classe;
    }

    public int GetInputForClasse()
    {
        int userInput = Convert.ToInt32(Console.ReadLine());
        while (IsInputNotValidForClasse(userInput))
        {
            this.ClearScreen();
            Console.WriteLine("Wrong Choice. Try again.");
            this.showClassOptions();
            userInput = Convert.ToInt32(Console.ReadLine());
        }
        this.ClearScreen();
        return userInput;
    }

    public bool IsInputNotValidForClasse(int userInput)
    {
        return userInput <= 0 || userInput >= 4;
    }

    private void showClassOptions()
    {
        Console.WriteLine("Whats your Class?");
        Console.WriteLine("1)Meele");
        Console.WriteLine("2)Ranged");
        Console.WriteLine("3)Healer");
    }

    public void ShowWelcomeMessage(String name, PlayerClasse classe)
    {
        Console.WriteLine("Welcome " + name + "! to Vlad's map!"
                + " You are a " + classe.Name);
    }

    public void ShowPlayerLocation(Point position)
    {
        Console.WriteLine("You are now at: (" + position.X + "," + position.Y
                + ")");
    }

    public void ShowPossibleDirections()
    {
        Console.WriteLine("Where do you want to go?");
        Console.WriteLine("1)North");
        Console.WriteLine("2)South");
        Console.WriteLine("3)East");
        Console.WriteLine("4)West");
    }

    public Direction GetDirection()
    {
        Direction userDirection = Direction.NORTH;
        switch (GetInputForDirection())
        {
            case 1:
                userDirection = Direction.NORTH;
                break;
            case 2:
                userDirection = Direction.SOUTH;
                break;
            case 3:
                userDirection = Direction.EAST;
                break;
            case 4:
                userDirection = Direction.WEST;
                break;
        }
        this.ClearScreen();
        return userDirection;
    }

    public int GetInputForDirection()
    {
        int userInput = Convert.ToInt32(Console.ReadLine());
        while (IsInputNotValidForDirection(userInput))
        {
            this.ClearScreen();
            Console.WriteLine("Wrong Choice");
            this.ShowPossibleDirections();
            userInput = Convert.ToInt32(Console.ReadLine());
        }
        return userInput;
    }

    public bool IsInputNotValidForDirection(int userInput)
    {
        return userInput <= 0 || userInput >= 5;
    }

    public void ShowAvailableActions()
    {
        Console.WriteLine(" What do you want to do now?");
        Console.WriteLine("1)Move");
        Console.WriteLine("2)Rest");
        Console.WriteLine("3)View Stats");
    }

    public void ShowPlayerStats(Player player)
    {
        Console.WriteLine("Player stats:");
        Console.WriteLine("Name: " + player.Name);
        Console.WriteLine("Class: " + player.Classe.Name);
        Console.WriteLine("Accuracy: " + player.Accuracy);
        Console.WriteLine("Hit Points: " + player.HitPoints);
        Console.WriteLine("Max Hit Points: " + player.MaxHitPoints);
        Console.WriteLine("Experience: " + player.XP);
        Console.WriteLine("xp for next Level: " + player.XpForNextLevel);
        Console.WriteLine("Current Level: " + player.Level);
        Console.WriteLine("Weapon: " + player.Weapon.Name);
        Console.WriteLine("Armor: " + player.Armor);
    }

    public void ShowPreRestMessage()
    {
        Console.WriteLine("You are now resting.....");
    }

    public void ShowPostRestMessage()
    {
        Console.WriteLine("You are fully rested!");
    }

    public void ShowEncounteredMonsterMessage(String monsterName)
    {
        Console.WriteLine("You Encountered a " + monsterName);
    }

    public void ShowNoMonsterMessage()
    {
        Console.WriteLine("You are lucky enough to not find any bugs!");
    }

    public void ShowMonsterHitPoints(String name, int hitPoints)
    {
        Console.WriteLine("The " + name + " has " + hitPoints + "hp");
    }

    public void ShowAvailableBattleActions()
    {
        Console.WriteLine("Here are the available options:");
        Console.WriteLine("1)Attack");
        Console.WriteLine("2)Run");
    }

    public void ShowRanSafelyFromMonsterMessage()
    {
        Console.WriteLine("You have ran away safely from the monster!");
    }

    public void ShowAfterPlayerAttackMonsterMessage(String name, int hitPoints)
    {
        Console.WriteLine("You have attacked the " + name);
        this.ShowMonsterHitPoints(name, hitPoints);
    }

    public void ShowAfterMonsterAttackPlayerMessage(String name, int damage)
    {
        Console.WriteLine("The " + name + " have attacked you and dealt "
                + damage + "hp of damage!");
    }

    public void ShowPlayerHPToString(int playerHP)
    {
        Console.WriteLine("You have now " + playerHP + "hp.");
    }

    public void ShowBattleVictoryMessage()
    {
        Console.WriteLine("You have won the battle!");
    }

    public void ShowLastDamageDoneOfPlayer(int damageDone)
    {
        Console.WriteLine("You have done " + damageDone + "hp of damage!");
    }

    public void ShowYouHaveDiedMessage()
    {
        Console.WriteLine("You have died! :/");
    }

    public void ClearScreen()
    {
        Console.Clear();
    }

    public void ShowThanksForPlayingMessage()
    {
        Console.WriteLine("Thanks for playing!");
        Console.WriteLine("Press Enter to continue....");
        Console.ReadLine();
    }

    public void ShowFailedPlayerAttack()
    {
        Console.WriteLine("You missed the attack! D:");
    }

    public void ShowFailedToScapeMessage()
    {
        Console.WriteLine("You failed to scape! GET READY TO KEEP BATTLEING!");
    }

    public void showNewLevelMessage(int newLevel)
    {
        Console.WriteLine("Congrats! you went up to the level: " + newLevel);
    }

    public void ShowFailedMonsterAttackMessage(String monsterName)
    {
        Console.WriteLine("You dodged the " + monsterName + " attack!");
    }
}
