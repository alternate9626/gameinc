﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    class StaffWeapon : Weapon
    {
        //TODO: WHY???
        //USE PROPERTIES, THIS ISNT JAVA!
        //FIX FOR ALL WEAPONS AND MONSTERS TOO!
        public String Name { get; private set; }
        public int MaxDamage { get; private set; }
        public int MinDamage { get; private set; }

        public StaffWeapon()
        {
            this.Name = "Staff";
            this.MaxDamage = 4;
            this.MinDamage = 1;
        }
    }
}
