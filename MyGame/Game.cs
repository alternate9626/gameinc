

using MyGame;
using System;
//TODO: REMOVE ALL THOSE HORRIBLE EMPTY LINES EVERYWHERE
//NOT JUST HERE EVERYWHERE, FROM NOW ON, NO EMPTY LINES ALLOWED (they're okay between methods BUT ONLY ONE!)!!!
public class Game
{
    private Player player;
    private UserInterface userInterface;
    private BattleStance battleStance;
    private Random rand = new Random();

    public Game(UserInterface ui)
    {
        this.userInterface = ui;
    }

    public void Start()
    {        
        player = new Player(userInterface.GetUserName(),
                userInterface.GetUserClasse());        
        userInterface.ShowWelcomeMessage(player.Name, player.Classe);
    }  

    public void Update()
    {
        userInterface.ShowAvailableActions();
        play(userInterface.GetUserAction());
        
        if (player.IsDead())
        {
            userInterface.ShowYouHaveDiedMessage();
            userInterface.ShowThanksForPlayingMessage();            
        }
    }

    private void play(int userAction)
    {
        switch (userAction)
        {
            case 1:
                move();
                break;
            case 2:
                rest();
                break;
            case 3:
                showStats();
                break;
        }
    }

    private void move()
    {
        userInterface.ShowPlayerLocation(player.Position);
        userInterface.ShowPossibleDirections();
        movePlayer(userInterface.GetDirection());

        //TODO: The whole battle system should be in another class, I mean wtf?
        //The game class is becoming way too big
        //and polluted with way too many different concerns (high level policies)
        //DO YOU EVEN REMEMBER WHAT THOSE ARE??
        if (isThereAMonster())
        {
            battleStance = new BattleStance(player,userInterface);
            battleStance.BattleARandomMonster();
        }        
    }

    private void movePlayer(Direction direction)
    {
        switch (direction)
        {
            case Direction.NORTH:
                player.MoveNorth();
                break;
            case Direction.SOUTH:
                player.MoveSouth();
                break;
            case Direction.EAST:
                player.MoveEast();
                break;
            case Direction.WEST:
                player.MoveWest();
                break;
            case Direction.STAY:
                break;
        }
    }

    private bool isThereAMonster()
    {        
        int randomNumber = rand.Next(2);
        return randomNumber == 1;
    }

    public bool IsPlayerAlive()
    {
        return !player.IsDead();
    }

    private void rest()
    {
        userInterface.ShowPreRestMessage();
        player.Rest();
        userInterface.ShowPostRestMessage();
    }

    private void teleportPlayerToSpawn()
    {
        Point position = new Point(0, 0);
        player.MoveTo(position);
        userInterface.ShowPlayerLocation(player.Position);
    }

    private void showStats()
    {
        userInterface.ShowPlayerStats(player);
    }

}