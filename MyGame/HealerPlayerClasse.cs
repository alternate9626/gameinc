using MyGame;
using System;
public class HealerPlayerClasse : PlayerClasse
{
    public int Accuracy { get; private set; }
    public int HitPoints { get; private set; }
    public int MaxHitPoints { get; private set; }
    public int XP { get; private set; }
    public int XpForNextLevel { get; private set; }
    public int Level { get; private set; }
    public int Armor { get; private set; }
    public Weapon Weapon { get; private set; }
    public String Name { get; private set; }
    public int Damage { get; private set; }

    public HealerPlayerClasse()
    {
        this.Accuracy = 8;
        this.HitPoints = 15;
        this.MaxHitPoints = 15;
        this.XP = 0;
        this.XpForNextLevel = 1000;
        this.Level = 1;
        this.Armor = 0;
        this.Weapon = new StaffWeapon();
        this.Name = "Healer";
        this.Damage = 5;
    }
}
