﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
   public  interface Weapon
    {
        String Name { get;  }
        int MaxDamage { get;  }
        int MinDamage { get;  }

    }
}
