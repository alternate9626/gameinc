using MyGame;
using System;
public interface PlayerClasse
{
    int Accuracy { get; }

    int Armor { get; }

    int HitPoints { get; }

    int Level { get; }

    int MaxHitPoints { get; }

    int XP { get; }

    int XpForNextLevel { get; }

    int Damage { get; }

    String Name { get; }

    Weapon Weapon { get; }

}
