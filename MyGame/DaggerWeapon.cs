﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    class DaggerWeapon : Weapon
    {
        public String Name { get; private set; }
        public int MaxDamage { get; private set; }
        public int MinDamage { get; private set; }

        public DaggerWeapon()
        {
            this.Name = "Dagger";
            this.MaxDamage = 5;
            this.MinDamage = 1;
        }     
    }
}
