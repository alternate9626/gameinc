﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    class LongSwordWeapon : Weapon
    {
        public String Name { get; private set; }
        public int MaxDamage { get; private set; }
        public int MinDamage { get; private set; }

        public LongSwordWeapon()
        {
            this.Name = "Long sword";
            this.MaxDamage = 8;
            this.MinDamage = 1;
        }
    }
}
