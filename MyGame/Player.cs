using MyGame;
using System;
public class Player
{
    public String Name { get; set; }
    public Point Position { get; set; }
    public PlayerClasse Classe { get; set; }
    public int Accuracy { get; set; }
    public int HitPoints { get; set; }
    public int MaxHitPoints { get; set; }
    public int XP { get; set; }
    public int XpForNextLevel { get; set; }
    public int Level { get; set; }
    public int Armor { get; set; }
    public int Damage { get; set; }
    public Weapon Weapon { get; set; }
    public int LastDamageDone { get; set; }
    public int LastDamageDoneByMonster { get; set; }
    private Random rand = new Random();

    public Player(String name, PlayerClasse classe)
    {
        this.Name = name;
        this.Classe = classe;
        this.setStats();
        Position = new Point(0, 0);
    }

    public void MoveNorth()
    {
        this.Position.Y++;
    }

    public void MoveSouth()
    {
        this.Position.Y--;
    }

    public void MoveEast()
    {
        this.Position.X++;
    }

    public void MoveWest()
    {
        this.Position.X--;
    }

    public void setStats()
    {
        this.Accuracy = Classe.Accuracy;
        this.HitPoints = Classe.HitPoints;
        this.MaxHitPoints = Classe.MaxHitPoints;
        this.XP = Classe.XP;
        this.XpForNextLevel = Classe.XpForNextLevel;
        this.Level = Classe.Level;
        this.Armor = Classe.Armor;
        this.Damage = Classe.Damage;
        this.Weapon = Classe.Weapon;
    }

    public void TakeDamage(int damageDone)
    {
        int totalDamage = damageDone - this.Armor;
        if(totalDamage < 0){
            totalDamage = 0;
        }
        this.LastDamageDoneByMonster = totalDamage;
        this.HitPoints = this.HitPoints - totalDamage;
        if (IsDead())
        {
            this.HitPoints = 0;
        }
    }

    public bool IsDead()
    {
        return this.HitPoints <= 0;
    }

    public bool IsAlive()
    {
        return !IsDead();
    }

    public void AttackMonster(Monster monster)
    {
       
            int damage = (rand.Next(this.Weapon.MinDamage, this.Weapon.MaxDamage) - monster.Armor);
            if (damage < 0)
            {
                damage = 0;
            }
            monster.TakeDamage(damage);
            this.LastDamageDone = damage;        
    }

    public void Rest()
    {
        this.HitPoints = this.MaxHitPoints;
    }

    public void MoveTo(Point position)
    {
        this.Position = position;
    }

    public void AddXpReward(int xpReward){
        this.XP = this.XP + xpReward;
        this.XpForNextLevel = this.XpForNextLevel - xpReward;
        if (isXpEnoughForNextLevel())
        {
            this.setStatsForNewLevel();
        }
    }

    private bool isXpEnoughForNextLevel()
    {
        return this.XP >= this.XpForNextLevel;
    }

    private void setStatsForNewLevel()
    {
        
        this.Level++;
        this.XpForNextLevel = this.Level * this.Level * 1000;
        this.Rest();
        this.Accuracy += rand.Next(1, 3);
        this.MaxHitPoints += rand.Next(2, 6);
        this.Armor += rand.Next(1, 2);
    }
    

}
