﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    class ShortSwordWeapon : Weapon
    {
        public String Name { get; private set; }
        public int MaxDamage { get; private set; }
        public int MinDamage { get; private set; }

        public ShortSwordWeapon()
        {
            this.Name = "Short Sword";
            this.MaxDamage = 7;
            this.MinDamage = 2;
        }
    }
}
