﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    class GameStarter
    {
        static void Main(string[] args)
        {
            MessagesUserInterface userInterface = new MessagesUserInterface();
            Game myGame = new Game(userInterface);
            myGame.Start();
            while (myGame.IsPlayerAlive())
            {
                myGame.Update();
            }
        }
    }
}
