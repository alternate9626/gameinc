using System;
public interface UserInterface
{      
    String GetUserName();

    PlayerClasse GetUserClasse();

    PlayerClasse GetClasse();

    void ShowWelcomeMessage(String name, PlayerClasse classe);

    void ShowPlayerLocation(Point position);

    void ShowPossibleDirections();

    Direction GetDirection();

    void ShowAvailableActions();

    int GetUserAction();

    void ShowPlayerStats(Player player);

    void ShowPreRestMessage();

    void ShowPostRestMessage();

    void ShowEncounteredMonsterMessage(String monsterName);

    void ShowNoMonsterMessage();

    void ShowMonsterHitPoints(String name, int hitPoints);

    void ShowAvailableBattleActions();

    void ShowRanSafelyFromMonsterMessage();

    void ShowAfterPlayerAttackMonsterMessage(String name, int hitPoints);

    void ShowAfterMonsterAttackPlayerMessage(String name, int damage);

    void ShowPlayerHPToString(int playerHP);

    void ShowBattleVictoryMessage();

    void ShowLastDamageDoneOfPlayer(int damageDone);

    void ShowYouHaveDiedMessage();

    void ClearScreen();

    void ShowThanksForPlayingMessage();

    void ShowFailedPlayerAttack();

    void ShowFailedToScapeMessage();

    void showNewLevelMessage(int newLevel);

    void  ShowFailedMonsterAttackMessage(String monsterName);
}