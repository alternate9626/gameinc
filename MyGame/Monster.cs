using System;
public interface Monster
{      
    //TODO: FUNCTION PARAMETERS DONT START CAPITALIZED!
    void AttackPlayer(Player player);
    void TakeDamage(int damageDone);
    void Kill();
    int HitPoints { get; }
    String Name { get; }
    int damage { get; }
    int Armor { get; }
    int LastDamageDone { get; }    
    int XpReward { get; }
    int Accuracy { get; }
}
