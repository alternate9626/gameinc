﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    class BattleStance
    {
        private Monster monster;
        private Player player;
        private UserInterface userInterface;
        private Random rand = new Random();

        public BattleStance(Player player , UserInterface userInterface)
        {
            this.player = player;
            this.userInterface = userInterface;
        }

        public void BattleARandomMonster()
        {
            monster = this.GetARandomMonster(); 
            this.userInterface.ShowEncounteredMonsterMessage(monster.Name);
            while (IsMonsterAlive() && IsPlayerAlive())
            {
                userInterface.ShowMonsterHitPoints(monster.Name,monster.HitPoints);
                userInterface.ShowAvailableBattleActions();
                BattleAMonster(userInterface.GetUserAction());
                userInterface.ShowPlayerHPToString(player.HitPoints);
            }            
        }       

        private Monster GetARandomMonster()
        {
            Monster monster = new GoblinMonster();            
            int randomNumber = rand.Next(1, 4);

            switch (randomNumber)
            {
                case 1:
                    monster = new OrcMonster();
                    break;
                case 2:
                    monster = new GoblinMonster();
                    break;
                case 3:
                    monster = new OgreMonster();
                    break;
                case 4:
                    monster = new OrcLordMonster();
                    break;
            }
            return monster;
        }

        private bool IsMonsterAlive()
        {
            return monster.HitPoints > 0;
        }

        private bool IsPlayerAlive()
        {
            return !player.IsDead();
        }

        private void BattleAMonster(int userAction)
        {
            switch (userAction)
            {
                case 1:
                    PlayerAttacksMonster();                    
                    break;
                case 2:
                    TryToRunAwayFromMonster();
                    break;
            }
        }

        private void PlayerAttacksMonster()
        {           
            if (rand.Next(1, 15) < player.Accuracy)
            {
                player.AttackMonster(monster);
                userInterface.ShowAfterPlayerAttackMonsterMessage(monster.Name,
                  monster.HitPoints);
                userInterface.ShowLastDamageDoneOfPlayer(player.LastDamageDone);
            }

            else
            {
                userInterface.ShowFailedPlayerAttack();
            }

            if (IsMonsterAlive())
            {
                MonsterAttacksPlayer();
            }
            else
            {
                int PlayerOldLevel = player.Level;
                player.AddXpReward(monster.XpReward);
                if (PlayerOldLevel != player.Level)
                {
                    userInterface.showNewLevelMessage(player.Level);
                }
                userInterface.ShowBattleVictoryMessage();
            }
        }

        private void MonsterAttacksPlayer()
        {            
            if (rand.Next(1, 10) < monster.Accuracy)
            {
                monster.AttackPlayer(player);
                userInterface.ShowAfterMonsterAttackPlayerMessage(monster.Name,
                        monster.LastDamageDone);
            }
            else
            {
                userInterface.ShowFailedMonsterAttackMessage(monster.Name);
            }
        }

        private void TryToRunAwayFromMonster()
        {
            if (this.rand.Next(1, 4) == 1)
            {
                userInterface.ShowRanSafelyFromMonsterMessage();
                monster.Kill();
            }
            else
            {
                userInterface.ShowFailedToScapeMessage();
            }

        }
        


    }
}
